import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { PostService }       from './post.service';
import { UserService }       from '../users/user.service';
import { User }              from '../users/user';
import * as _ from 'underscore'; 

@Component({
    templateUrl: './posts.component.html',
})
export class PostsComponent implements OnInit {
    games = ["World of Warcraft"];
    teams = ["Squad","Group"];
    ems = ["Event","Match"];
    Request: User[] = [];
    requests: FirebaseListObservable<any[]>;
    public uid="";
    // count: string;
    // nRequest: string;
    constructor(
        public af: AngularFire,
        private _postService: PostService,
        private _userService: UserService) {
	}

	ngOnInit() {
	     this.requests = this.af.database.list('/requests/');
	}
	
	create(gs,tt,em,stm,dua,dcp){
	   // this.count=this.count+1;
	   // this.nRequest="Request"+'count';
	   // console.log(this.nRequest);
	    this.Request.push(
            new User(gs, tt, em, stm, dua, dcp)
        );
	   // this.user.game=gs;
	   // this.user.team=tt;
    // 	this.user.em=em;
    // 	this.user.startime=stm;
    // 	this.user.duration=dua;
    // 	this.user.discription=dcp;
	    this.requests.push({
	        Request:{
	            game:gs,
	            team:tt,
	            em:em,
	            startime:stm,
	            duration:dua,
	            discription:dcp
	        }
	    });
    }
    
    
    deleterequest(uid){
        this.uid=uid.$key;
        console.log(this.uid);
        // console.log('/requests/' + this.uid);
        this.af.database.object('/requests/'+ this.uid).remove();
    }
    
    
}