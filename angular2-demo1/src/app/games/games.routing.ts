
import { RouterModule  }     from '@angular/router';

import { GamesComponent }    from './games.component';
import { GameDetailComponent } 		  from './game-detail.component';
export const gamesRouting = RouterModule.forChild([
    
    { path: 'games/detail', component: GameDetailComponent },
    { path: 'games', component: GamesComponent }
]);