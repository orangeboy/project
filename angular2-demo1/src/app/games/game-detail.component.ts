import { Component, OnInit }                     from '@angular/core';
import { Router }                from '@angular/router';
import { AngularFire, AuthProviders, AuthMethods,FirebaseListObservable} from 'angularfire2';
import { UsersComponent } from '../users/users.component';
import { UserService }       from '../users/user.service';
@Component({
    templateUrl: './game-detail.component.html'
})

export class GameDetailComponent{
    public isLoggedIn: boolean
    public uid='';
    constructor(public af: AngularFire,public ss: UserService){
        this.isLoggedIn=this.ss.globalislog;
        this.uid=this.ss.globaluid;
        
    }
    
    Add(){
        this.af.database.object('/users/' + this.uid).update({
            game:'World of Warcraft'
        })
        alert("Game Added!");
    }
}