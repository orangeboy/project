import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { GamesComponent }      from './games.component';
import { RouterModule }        from '@angular/router';
import { GameDetailComponent } from './game-detail.component'
@NgModule({
    imports: [
        CommonModule,
        RouterModule,
    ],
    declarations: [
        GamesComponent,
        GameDetailComponent,
    ],
    exports: [
        GamesComponent,
        GameDetailComponent,
    ],
})
export class GamesModule { 
}