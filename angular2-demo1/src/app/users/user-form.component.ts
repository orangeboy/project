import { Component, OnInit}                     from '@angular/core';
import { FormBuilder, FormGroup, Validators }    from '@angular/forms';
import { Router, ActivatedRoute }                from '@angular/router';
import { BasicValidators }                       from '../shared/basicValidators';
import { UserService }                           from './user.service';
import { User }                                  from './user';
import { AngularFire, AuthProviders, AuthMethods,FirebaseListObservable} from 'angularfire2';



@Component({
    templateUrl: './user-form.component.html'
})
export class UserFormComponent implements OnInit {
// 	form: FormGroup;
// title: string;
// user = new User();
    games = ["Alliance","Horde"];
    modes = ["PVP","PVE","Casual player"];

	constructor(public af: AngularFire,public ss: UserService) {
// 		this.form = fb.group({
// 			name: ['', Validators.required],
// 			email: ['', BasicValidators.email],
// 			phone: [],
// 			address: fb.group({
// 				street: [],
// 				suite: [],
// 				city: [],
// 				zipcode: []
// 			})
// 		});
	}
    
    ngOnInit(){
        this.af.auth.subscribe(authState => {})
//         var id = this._route.params.subscribe(params => {
//             var id = +params["id"];

//               this.title = id ? "Edit User" : "New User";
        
//         if (!id)
// 			return;
            
//         this._userService.getUser(id)
// 			.subscribe(
//                 user => this.user = user,
//                 response => {
//                     if (response.status == 404) {
//                         this._router.navigate(['NotFound']);
//                     }
//                 });
//         });
    }
    
    save(nam,age,pid,rpt,sgc,gs,cha){
        console.log('/users/' + this.ss.globaluid);
        this.af.database.object('/users/' + this.ss.globaluid).update({
            Name:nam,
            Age:age,
            PlayerID:pid,
            Regularplaytime: rpt,
            SupportedGamesCamp: sgc,
            GamesStyle: gs,
            // Characters: cha
        })
        
        alert("Profile Updated!");
  }
}