import { Component, OnInit, Inject } from '@angular/core';
import { UserService }       from './user.service';
import { UserFormComponent }   from './user-form.component';
import { AngularFire, AuthProviders, AuthMethods, FirebaseApp} from 'angularfire2';


@Component({
    templateUrl: './users.component.html'
    
})
export class UsersComponent implements OnInit {
    users: any[];
    displayName;
    photoURL;
    error;
    public isLoggedIn: boolean
    private au: any;
    
    constructor(public ss: UserService, public af: AngularFire, @Inject(FirebaseApp) fa : any){
		this.au = fa.auth();
	}

	ngOnInit(){
		// this._service.getUsers().subscribe(users => this.users = users);
		this.isLoggedIn = false;
		
		this.af.auth.subscribe(authState => {
			if(!authState){
				this.displayName = null;
				this.photoURL = null;
				this.isLoggedIn = false;
				this.ss.globalislog=false;
				return;
			}
		this.displayName = authState.auth.displayName;
		this.photoURL = authState.auth.photoURL;
		this.isLoggedIn = true;
		this.ss.globalislog=true;
		this.ss.globaluid=authState.uid;
		console.log(this.ss.globaluid,this.ss.globalislog);
		});
		
		// this.subscription = this._service.getEmittedValue().subscribe(isLoggedIn => this.isLoggedIn=isLoggedIn);

		
	} 
	
	login(eml,psw){
		this.af.auth.login({
			email: eml,
			password: psw
			},
			{
			method: AuthMethods.Password,
			provider: AuthProviders.Password
			}).then(authState => console.log("login-then",authState))
		.catch(error => console.log("login-error",error));
	}
	
	loginfacebook(){
		this.af.auth.login({
			provider: AuthProviders.Facebook,
			method: AuthMethods.Popup
		}).then((authState: any)=> {
			this.af.database.object('/users/' + authState.uid).update({
				accessToken: authState.facebook.accessToken
			});
		});
	}
	
	register(eml,psw){
		this.af.auth.createUser({
			email: eml,
			password: psw
		})
		.then((authState :any)=> {
			alert("Account Created!");
			authState.auth.sendEmailVerification();
			this.error="";
			this.af.database.object('/users/' + authState.uid).update({
				accessToken: authState.uid
			});
		})
		.catch(error => this.error = error.message);
	}
	
	reset(eml){
		if(eml!="")
			this.au.sendPasswordResetEmail(eml)
	.then(
		alert("Password Reset Email Sent!")
	).catch( error => console.log('failed to send', error));
	}
	
	logout(){
		this.af.auth.logout();
    }
}



























 //   deleteUser(user){
	// 	if (confirm("Are you sure you want to delete " + user.name + "?")) {
	// 		var index = this.users.indexOf(user)
	// 		// Here, with the splice method, we remove 1 object
 //           // at the given index.
 //           this.users.splice(index, 1);

	// 		this._service.deleteUser(user.id)
	// 			.subscribe(null, 
	// 				err => {
	// 					alert("Could not delete the user.");
 //                       // Revert the view back to its original state
 //                       // by putting the user object at the index
 //                       // it used to be.
	// 					this.users.splice(index, 0, user);
	// 				});
	// 	}
	// }