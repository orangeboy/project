import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: './app.component.html',
  
})
export class AppComponent {
  cuisines: FirebaseListObservable<any[]>;
  restaurant;
  constructor(private af: AngularFire){}
    
    //show object
    // ngOnInit(){
    // this.cuisines = this.af.database.list('/cuisines');
    // this.restaurant = this.af.database.object('/restaurant');
    // }
    
    //add object
    add(){
      this.cuisines.push({
        name: 'Asian',
        details:{
          des:'...'
        }
      });
    }
    
    //update object
    update(){
      this.af.database.object('/restaurant').update({
        name: 'New Name',
        rating: 5
        });
      }
}