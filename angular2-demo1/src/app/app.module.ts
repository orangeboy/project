import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AppComponent } from './app.component';

import { SharedModule }      from './shared/shared.module';
import { UsersModule }       from './users/users.module';
import { PostsModule }       from './posts/posts.module';
import { GamesModule }       from './games/games.module';


import { routing } from './app.routing';

import { HomeComponent }     from './home.component';
import { NavBarComponent }   from './navbar.component';

import { usersRouting }      from './users/users.routing';
import { postsRouting }      from './posts/posts.routing';
import { gamesRouting }      from './games/games.routing';
export const firebaseConfig = {
    apiKey: "AIzaSyDKw2PhFpwsL9uJArHWEM_Q_4RB6hmH6LY",
    authDomain: "project-12c97.firebaseapp.com",
    databaseURL: "https://project-12c97.firebaseio.com",
    storageBucket: "project-12c97.appspot.com",
    messagingSenderId: "12919586019"
};

@NgModule({
  declarations: [
    AppComponent,
     HomeComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    SharedModule,
    UsersModule,
    PostsModule,
    GamesModule,
    gamesRouting,
    usersRouting,
    postsRouting,
    routing,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
